import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inspireui/utils/screen_utils.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'common/config.dart';
import 'common/constants.dart';
import 'models/app.dart';
import 'models/blogs/blog.dart';
import 'models/cart/cart_model.dart';
import 'models/category/category_model.dart';
import 'models/filter_attribute.dart';
import 'models/filter_tags.dart';
import 'screens/home/onboard_screen.dart';
import 'screens/users/login.dart';
import 'services/index.dart';
import 'widgets/common/animated_splash.dart';
import 'widgets/common/dialogs.dart';
import 'widgets/common/internet_connectivity.dart';
import 'widgets/common/static_splashscreen.dart';

class AppInit extends StatefulWidget {
  final Function onNext;

  AppInit({this.onNext});

  @override
  _AppInitState createState() => _AppInitState();
}

class _AppInitState extends State<AppInit> with AfterLayoutMixin {
  final StreamController<bool> _streamInit = StreamController<bool>();

  bool isFirstSeen = false;
  bool isLoggedIn = false;
  Map appConfig = {};

  /// check if the screen is already seen At the first time
  Future<bool> checkFirstSeen() async {
    /// Ignore if OnBoardOnlyShowFirstTime is set to true.
    if (kAdvanceConfig['OnBoardOnlyShowFirstTime'] == false) {
      return false;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = prefs.getBool('seen') ?? false;
    return _seen;
  }

  /// Check if the App is Login
  Future checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('loggedIn') ?? false;
  }

  Future loadInitData() async {
    try {
      printLog("[AppState] Inital Data");
      isFirstSeen = await checkFirstSeen();
      setState(() {});

      isLoggedIn = await checkLogin();

      /// Load App model config
      Services().setAppConfig(serverConfig);
      appConfig =
          await Provider.of<AppModel>(context, listen: false).loadAppConfig();
      Provider.of<CartModel>(context, listen: false).changeCurrencyRates(
          Provider.of<AppModel>(context, listen: false).currencyRate);

      Future.delayed(Duration.zero, () {
        /// Load more Category/Blog/Attribute Model beforehand
        if (mounted) {
          Provider.of<CategoryModel>(context, listen: false).getCategories(
            lang: Provider.of<AppModel>(context, listen: false).locale,
            cats: Provider.of<AppModel>(context, listen: false).categories,
          );

          Provider.of<BlogModel>(context, listen: false).getBlogs();

          Provider.of<FilterTagModel>(context, listen: false).getFilterTags();

          Provider.of<FilterAttributeModel>(context, listen: false)
              .getFilterAttributes();
        }
      });

      printLog("[AppState] Init Data Finish");
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
    if (!_streamInit.isClosed) {
      _streamInit.add(isFirstSeen);
    }
  }

  Widget onNextScreen(bool isFirstSeen) {
    if (!isFirstSeen && !kIsWeb && appConfig != null) {
      if (onBoardingData.isNotEmpty) return OnBoardScreen(appConfig);
    }

    if (kLoginSetting['IsRequiredLogin'] && !isLoggedIn) {
      return LoginScreen(
        onLoginSuccess: (context) async {
          await Navigator.pushNamed(context, RouteList.home);
        },
      );
    }
    return widget.onNext();
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
    Future.delayed(
      const Duration(seconds: 1),
      () {
        printLog("[AppState] init mobile modules ..");

        MyConnectivity.instance.initialise();
        MyConnectivity.instance.myStream.listen((onData) {
          printLog("[App] internet issue change: $onData");
          if (MyConnectivity.instance.isIssue(onData)) {
            if (MyConnectivity.instance.isShow == false) {
              MyConnectivity.instance.isShow = true;
              showDialogNotInternet(context).then((onValue) {
                MyConnectivity.instance.isShow = false;
                printLog("[showDialogNotInternet] dialog closed $onValue");
              });
            }
          } else {
            if (MyConnectivity.instance.isShow == true) {
              Navigator.of(context).pop();
              MyConnectivity.instance.isShow = false;
            }
          }
        });
        printLog("[AppState] register modules .. DONE");
      },
    );
  }

  @override
  void dispose() {
    _streamInit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: _streamInit.stream,
      builder: (context, snapshot) {
        var _isPushNext = snapshot.data != null;

        String splashScreenType = appConfig['SplashScreen'] != null
            ? appConfig['SplashScreen']['type']
            : kSplashScreenType;
        dynamic splashScreenData = appConfig['SplashScreen'] != null
            ? appConfig['SplashScreen']['data']
            : kSplashScreen;

        if (splashScreenType == 'flare') {
          return SplashScreen.navigate(
            name: splashScreenData,
            startAnimation: 'fluxstore',
            backgroundColor: Colors.white,
            next: (object) => onNextScreen(isFirstSeen),
            until: () => Future.delayed(const Duration(seconds: 2)),
          );
        }

        if (splashScreenType == 'animated') {
          debugPrint('[FLARESCREEN] Animated');
          return AnimatedSplash(
            imagePath: splashScreenData,
            home: onNextScreen(isFirstSeen),
            duration: 2500,
            type: AnimatedSplashType.StaticDuration,
            isPushNext: _isPushNext,
          );
        }
        if (splashScreenType == 'zoomIn') {
          return CustomSplash(
            imagePath: splashScreenData,
            backGroundColor: Colors.white,
            animationEffect: 'zoom-in',
            logoSize: 50,
            home: onNextScreen(isFirstSeen),
            duration: 2500,
          );
        }
        if (splashScreenType == 'static') {
          return StaticSplashScreen(
            imagePath: splashScreenData,
            onNextScreen: onNextScreen(isFirstSeen),
          );
        }
        return Container();
      },
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    ScreenUtil.init(context);
  }
}
