import 'package:flutter/material.dart';

import '../../common/config.dart';
import '../../common/tools.dart';
import 'banner/banner_animate_items.dart';
import 'banner/banner_group_items.dart';
import 'banner/banner_slider_items.dart';
import 'category/category_icon_items.dart';
import 'category/category_image_items.dart';
import 'header/header_search.dart';
import 'header/header_text.dart';
import 'horizontal/blog_list_items.dart';
import 'horizontal/featured_vendors_layout.dart';
import 'horizontal/horizontal_list_items.dart';
import 'horizontal/instagram_items.dart';
import 'horizontal/simple_list.dart';
import 'horizontal/video/index.dart';
import 'logo.dart';
import 'product_list_layout.dart';

class DynamicLayout extends StatelessWidget {
  final config;
  final setting;

  DynamicLayout(this.config, this.setting);

  @override
  Widget build(BuildContext context) {
    switch (config["layout"]) {
      case "logo":
        final isTablet = Tools.isTablet(MediaQuery.of(context));

        if (setting["StickyHeader"] != null &&
            setting["StickyHeader"] &&
            !isTablet) {
          return Container(height: 40);
        }

        return Logo(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case 'header_text':
        if (kLayoutWeb) return Container();
        return HeaderText(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case 'header_search':
        if (kLayoutWeb) return Container();
        return HeaderSearch(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );
      case 'featuredVendors':
        if (kLayoutWeb) return Container();
        return FeaturedVendorsLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );
      case "category":
        return (config['type'] == 'image')
            ? CategoryImages(
                config: config,
                key: config['key'] != null ? Key(config['key']) : null,
              )
            : CategoryIcons(
                config: config,
                key: config['key'] != null ? Key(config['key']) : null,
              );

      case "bannerAnimated":
        if (kLayoutWeb) return Container();
        return BannerAnimated(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "bannerImage":
        if (config['isSlider'] == true) {
          return BannerSliderItems(
              config: config,
              key: config['key'] != null ? Key(config['key']) : null);
        }
        return BannerGroupItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "largeCardHorizontalListItems":
        return LargeCardHorizontalListItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "simpleVerticalListItems":
        return SimpleVerticalProductList(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "instagram":
        return InstagramItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "blog":
        return BlogListItems(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      case "video":
        return VideoLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );

      default:
        return ProductListLayout(
          config: config,
          key: config['key'] != null ? Key(config['key']) : null,
        );
    }
  }
}
