import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../../../vendor/store_detail/store_detail_screen.dart';
import '../../../common/constants.dart';
import 'package:provider/provider.dart';

import '../../../common/tools.dart';
import '../../../vendor/store/store_model.dart';
import '../../common/start_rating.dart';
import '../header/header_view.dart';

class FeaturedVendorsLayout extends StatefulWidget {
  final config;

  FeaturedVendorsLayout({this.config, Key key}) : super(key: key);

  @override
  _FeaturedVendorsLayoutState createState() => _FeaturedVendorsLayoutState();
}

class _FeaturedVendorsLayoutState extends State<FeaturedVendorsLayout> {
  int displayColumnCount;
  @override
  void initState() {
    super.initState();
    displayColumnCount = widget.config['columnCount'] ?? 3;
  }

  Widget featuredItem({String name, double rating, String imgUrl}) {
    final ThemeData theme = Theme.of(context);
    final isTablet = Tools.isTablet(MediaQuery.of(context));

    double titleFontSize = isTablet ? 18.0 : 14.0;
    double ratingCountFontSize = isTablet ? 16.0 : 12.0;
    double starSize = isTablet ? 16.0 : 10.0;
    String _defaultImage = imgUrl ??
        "https://media.istockphoto.com/photos/vintage-retro-grungy-background-design-and-pattern-texture-picture-id656453072?k=6&m=656453072&s=612x612&w=0&h=4TW6UwMWJrHwF4SiNBwCZfZNJ1jVvkwgz3agbGBihyE=";
    return Container(
      width: 100.0,
      height: 150.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Container(
              color: Colors.black26,
              child: Image.network(
                _defaultImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                name,
                style: TextStyle(
                  fontSize: titleFontSize,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Align(
              alignment: Alignment.centerLeft,
              child: SmoothStarRating(
                  allowHalfRating: true,
                  starCount: 5,
                  label: Text(
                    '0',
                    style: TextStyle(fontSize: ratingCountFontSize),
                  ),
                  rating: 5.0,
                  size: starSize,
                  color: theme.primaryColor,
                  spacing: 0.0),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    int column = displayColumnCount;
    return FutureBuilder(
        future:
            Provider.of<StoreModel>(context, listen: false).getFeaturedStores(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  HeaderView(headerText: widget.config['name']),
                  Container(
                    height: 150,
                    color: Colors.transparent,
                    child: Swiper(
                      itemBuilder: (context, swiperIndex) {
                        int listCardLength = column;
                        if (snapshot.data.length % column != 0) {
                          if (swiperIndex ==
                              (snapshot.data.length / column).floor()) {
                            listCardLength = snapshot.data.length % column;
                          }
                        }
                        return Center(
                          child: Wrap(
                            spacing: 20.0,
                            runSpacing: 10.0,
                            children: List.generate(
                              listCardLength,
                              (index) {
                                Store store =
                                    snapshot.data[index + column * swiperIndex];
                                return InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      RouteList.storeDetail,
                                      arguments:
                                          StoreDetailArgument(store: store),
                                    );
                                  },
                                  child: featuredItem(
                                    name: store.name,
                                    rating: store.rating,
                                    imgUrl: store.image,
                                  ),
                                );
                              },
                            ),
                          ),
                        );
                      },
                      itemCount: (snapshot.data.length % column) == 0
                          ? (snapshot.data.length / column).round()
                          : (snapshot.data.length / column).floor() + 1,
                    ),
                  ),
                ],
              );
            } else {
              return Container();
            }
          }
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              HeaderView(headerText: widget.config['name']),
            ],
          );
        });
  }
}
