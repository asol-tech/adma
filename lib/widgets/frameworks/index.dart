import 'package:flutter/material.dart';

import '../../models/cart/cart_model.dart';
import '../../models/country.dart' as country_model;
import '../../models/order/order.dart';
import '../../models/payment_method.dart';
import '../../models/product/product.dart';
import '../../models/product/product_variation.dart';
import '../../models/user/user_model.dart';

export 'woocommerce/index.dart';

abstract class BaseFrameworks {
  bool get enableProductReview;

  Future<void> doCheckout(context,
      {Function success, Function error, Function loading});

  Future<void> applyCoupon(context,
      {String code, Function success, Function error});

  Future<void> createOrder(context,
      {Function onLoading,
      Function success,
      Function error,
      paid = false,
      cod = false});

  void placeOrder(context,
      {CartModel cartModel,
      PaymentMethod paymentMethod,
      Function onLoading,
      Function success,
      Function error});

  Map<String, dynamic> getPaymentUrl(context);

  /// For Cart Screen
  Widget renderCartPageView(
      {isModal, isBuyNow, pageController, BuildContext context});

  Widget renderVariantCartItem(variation);

  String getPriceItemInCart(Product product, ProductVariation variation,
      Map<String, dynamic> currencyRate, String currency);

  /// For Update User Screen
  void updateUserInfo(
      {User loggedInUser,
      context,
      onError,
      onSuccess,
      currentPassword,
      userDisplayName,
      userEmail,
      userNiceName,
      userUrl,
      userPassword});

  Widget renderCurrentPassInputforEditProfile({context, currentPassword});

  /// For app model
  Future<void> onLoadedAppConfig(callback);

  /// For Shipping Address checkout
  void loadShippingMethods(context, CartModel cartModel, bool beforehand);

  /// For Order Detail Screen
  Future<Order> cancelOrder(BuildContext context, Order order);

  Widget renderButtons(Order order, cancelOrder, createRefund);

  /// For product variant
  Future<void> getProductVariantions(
      {context,
      Product product,
      onLoad({productInfo, variations, mapAttribute, variation})});

  bool couldBePurchased(productVariation, Product product, mapAttribute);

  void onSelectProductVariant(attr, val, variations, mapAttribute, onFinish);

  List<Widget> getProductAttributeWidget(
      lang, Product product, mapAttribute, onSelectProductVariant);

  List<Widget> getProductTitleWidget(
      context, productVariation, Product product);

  List<Widget> getBuyButtonWidget(context, productVariation, Product product,
      mapAttribute, maxQuantity, quantity, addToCart, onChangeQuantity);

  void addToCart(context, Product product, quantity, productVariation,
      [buyNow = false, bool inStock = false]);

  /// Load countries for shipping address
  Future<List<country_model.Country>> loadCountries(BuildContext context);

  /// Load states for shipping address
  Future<List<country_model.State>> loadStates(country_model.Country country);

  Product updateProductObject(Product product, Map json);
  
  Future<void> resetPassword(context, String username);

  Widget renderShippingPaymentTitle(context, String title);

  Future<Product> getProductDetail(context, Product product);
}
