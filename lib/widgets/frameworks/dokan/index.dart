import '../woocommerce/index.dart';
import '../../../models/product/product.dart';
import '../../../vendor/store/store_model.dart';
import '../../../vendor/checkout/vendor_shipping_model.dart';
import '../../../models/cart/cart_model.dart';
import 'package:provider/provider.dart';
import '../../../common/config/payments.dart';

class DokanWidget extends WooWidget {
  static final DokanWidget _instance = DokanWidget._internal();
  factory DokanWidget() => _instance;
  DokanWidget._internal();

  @override
  Product updateProductObject(Product product, Map json) {
    product.store = Store.fromDokanJson(json["store"]);
    return product;
  }

  @override
  void loadShippingMethods(context, CartModel cartModel, bool beforehand) {
    Future.delayed(Duration.zero, () {
      Map<String, Store> results = Map<String, Store>();
      if (kPaymentConfig["DisableVendorShipping"] == false) {
        final item = Provider.of<CartModel>(context, listen: false).item;
        item.values.toList().forEach((Product product) {
          if (product.store != null &&
              product.store.id != null &&
              results[product.store.id.toString()] == null) {
            results[product.store.id.toString()] = product.store;
          } else {
            results["-1"] = null;
          }
        });
      } else {
        results["-1"] = null;
      }
      Provider.of<VendorShippingMethodModel>(context, listen: false)
          .getShippingMethods(
              cartModel: cartModel, stores: results.values.toList());
    });
  }
}
