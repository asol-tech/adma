import 'package:flutter/material.dart';
import '../../models/category/category.dart';
import 'menu_card.dart';

class HorizonMenu extends StatefulWidget {
  final List<Category> categories;
  HorizonMenu(this.categories);

  @override
  _StateHorizonMenu createState() => _StateHorizonMenu();
}

class _StateHorizonMenu extends State<HorizonMenu> {
  List<Category> getCategory() {
    final categories = widget.categories;
    return categories.where((item) => item.parent == '0').toList();
  }

  List getChildrenOfCategory(Category category) {
    final categories = widget.categories;
    var children = categories.where((o) => o.parent == category.id).toList();
    return children;
  }

  @override
  Widget build(BuildContext context) {
    final categories = getCategory();

    return Column(
      children: List.generate(
        categories.length,
        (index) {
          return MenuCard(
              getChildrenOfCategory(categories[index]), categories[index]);
        },
      ),
    );
  }
}
