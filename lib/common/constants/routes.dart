/// route list constant
class RouteList {

  // Home
  static const String home = '/home';
  static const String homeScreen = '/home-screen';
  static const String homeSearch = '/home-search';

  static const String login = '/login';
  static const String register = '/register';

  // vendor
  static const String createProduct = '/create-product';
  static const String storeDetail = '/store-detail';
  static const String productSell = '/product-sell';
  static const String listChat = '/list-chat';
  static const String map = '/map';
}
