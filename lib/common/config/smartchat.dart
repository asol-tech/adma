import 'package:font_awesome_flutter/font_awesome_flutter.dart';

const kConfigChat = {
  "EnableSmartChat": true,
};

/// config for the chat app
/// config Whatapp: https://faq.whatsapp.com/en/iphone/23559013
const smartChat = [
  {
    'app': 'https://wa.me/00966559719767',
    'iconData': FontAwesomeIcons.whatsapp
  },
  {
    'app': 'https://wa.me/00966541707191',
    'iconData': FontAwesomeIcons.whatsapp
  },
  {'app': 'tel:8499999999', 'iconData': FontAwesomeIcons.phone},
  {'app': 'sms://8499999999', 'iconData': FontAwesomeIcons.sms}
  /* {
    'app': 'https://tawk.to/chat/5e5cab81a89cda5a1888d472/default',
    'iconData': FontAwesomeIcons.facebookMessenger
  } */
];
const String adminEmail = "admaa.app@gmail.com";
