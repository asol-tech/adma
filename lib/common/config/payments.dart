/**
 * Everything Config about the Payment
 */

/// config for payment features
const kPaymentConfig = {
  "DefaultCountryISOCode": "VN",

  "DefaultStateISOCode": "SG",

  /// Enable the Shipping optoion from Checkout, support for the Digital Download
  "EnableShipping": true,

  /// Enable the Google Map picker
  "EnableAddress": true,

  /// Enable the product review option
  "EnableReview": true,

  /// enable the google map picker from Billing Address
  'allowSearchingAddress': true,
  "GuestCheckout": true,

  /// Enable Payment option
  "EnableOnePageCheckout": false,
  "NativeOnePageCheckout": true,

  /// This config is same with checkout page slug in the website
  "CheckoutPageSlug": {"en": "checkout"},

  /// Enable Credit card payment (only available for Fluxstore Shopipfy)
  "EnableCreditCard": false,

  /// Enable update order status to processing after checkout by COD on woo commerce
  "UpdateOrderStatus": false,

  /// Show Refund and Order button on Order Detail
  "EnableRefundCancel": false,

  /// Disable show shipping methods by vendor
  "DisableVendorShipping": false
};

const Payments = {
  "paypal": "assets/icons/payment/paypal.png",
  "stripe": "assets/icons/payment/stripe.png",
  "razorpay": "assets/icons/payment/razorpay.png",
};

const PaypalConfig = {
  "clientId":
      "ASlpjFreiGp3gggRKo6YzXMyGM6-NwndBAQ707k6z3-WkSSMTPDfEFmNmky6dBX00lik8wKdToWiJj5w",
  "secret":
      "ECbFREri7NFj64FI_9WzS6A0Az2DqNLrVokBo0ZBu4enHZKMKOvX45v9Y1NBPKFr6QJv2KaSp5vk5A1G",
  "production": false,
  "paymentMethodId": "paypal", // change "paypal" -> "ppec_paypal"
  "enabled": true,
  "returnUrl": "http://return.example.com",
  "cancelUrl": "http://cancel.example.com",
};

const RazorpayConfig = {
  "keyId": "rzp_test_Iz3ByJRZoHMgxr",
  "paymentMethodId": "razorpay",
  "enabled": true
};

const TapConfig = {
  "SecretKey": "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
  "RedirectUrl": "http://your_website.com/redirect_url",
  "paymentMethodId": "",
  "enabled": false
};

/// config for after shipping
const afterShip = {
  "api": "e2e9bae8-ee39-46a9-a084-781d0139274f",
  "tracking_url": "https://fluxstore.aftership.com"
};

/// Limit the country list from Billing Address
/// []: default show all country
const List DefaultCountry = [];

//const List DefaultCountry = [
//  {
//    "name": "Vietnam",
//    "iosCode": "VN",
//    "icon": "https://cdn.britannica.com/41/4041-004-A06CBD63/Flag-Vietnam.jpg"
//  },
//  {
//    "name": "India",
//    "iosCode": "IN",
//    "icon":
//        "https://upload.wikimedia.org/wikipedia/en/thumb/4/41/Flag_of_India.svg/1200px-Flag_of_India.svg.png"
//  },
//  {"name": "Austria", "iosCode": "AT", "icon": ""},
//];
