export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

const serverConfig = {
  "type": "dokan",
  "url": "http://multivendor.demo.asol-tec.com",
  "consumerKey": "ck_5b1d8398237c9521777b89d16ab1754297164507",
  "consumerSecret": "cs_381a7114f70bb87e2e8f5afff303c1c6f52df173",
  "blog": "http://multivendor.demo.asol-tec.com",
  "forgetPassword":
      "http://multivendor.demo.asol-tec.com/wp-login.php?action=lostpassword",
};
