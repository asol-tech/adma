import 'dart:async';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'app_init.dart';
import 'common/constants.dart';
import 'common/styles.dart';
import 'common/tools.dart';
import 'generated/l10n.dart';
import 'models/advertisement.dart';
import 'models/app.dart';
import 'models/blogs/blog.dart';
import 'models/cart/cart_model.dart';
import 'models/category/category_model.dart';
import 'models/filter_attribute.dart';
import 'models/filter_tags.dart';
import 'models/notification.dart';
import 'models/order/order_model.dart';
import 'models/payment_method.dart';
import 'models/product/product_model.dart';
import 'models/recent_product.dart';
import 'models/shipping_method.dart';
import 'models/user/user_model.dart';
import 'models/wishlist.dart';
import 'vendor/store/store_model.dart';
import 'route.dart';
import 'routes/route_observer.dart';
import 'tabbar.dart';
import 'widgets/firebase/firebase_analytics_wapper.dart';
import 'widgets/firebase/firebase_cloud_messaging_wapper.dart';
import 'widgets/firebase/one_signal_wapper.dart';
import 'vendor/checkout/vendor_shipping_model.dart';

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppState();
  }
}

class AppState extends State<App> implements FirebaseCloudMessagingDelegate {
  final _app = AppModel();
  final _product = ProductModel();
  final _wishlist = WishListModel();
  final _shippingMethod = ShippingMethodModel();
  final _paymentMethod = PaymentMethodModel();
  final _advertisementModel = Ads();
  final _order = OrderModel();
  final _recent = RecentModel();
  final _store = StoreModel();
  final _vendorShippingMethod = VendorShippingMethodModel();
  final _blog = BlogModel();
  final _user = UserModel();
  final _filterModel = FilterAttributeModel();
  final _filterTagModel = FilterTagModel();
  final _categoryModel = CategoryModel();
  CartInject cartModel = CartInject();
  bool isFirstSeen = false;
  bool isLoggedIn = false;

  FirebaseAnalyticsAbs firebaseAnalyticsAbs;

  @override
  void initState() {
    printLog("[AppState] initState");

    if (kIsWeb) {
      printLog("[AppState] init WEB");
      firebaseAnalyticsAbs = FirebaseAnalyticsWeb();
    } else {
      firebaseAnalyticsAbs = FirebaseAnalyticsWapper()..init();

      Future.delayed(
        const Duration(seconds: 1),
        () {
          printLog("[AppState] init mobile modules ..");

          FirebaseCloudMessagagingWapper()
            ..init()
            ..delegate = this;

          OneSignalWapper()..init();
          printLog("[AppState] register modules .. DONE");
        },
      );
    }

    super.initState();
  }

  void _saveMessage(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      _app.deeplink = message['data'];
    }

    FStoreNotification a = FStoreNotification.fromJsonFirebase(message);
    final id = message['notification'] != null
        ? message['notification']['tag']
        : message['data']['google.message_id'];

    a.saveToLocal(id);
  }

  @override
  void onLaunch(Map<String, dynamic> message) {
    printLog('[app.dart] onLaunch Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onMessage(Map<String, dynamic> message) {
    printLog('[app.dart] onMessage Pushnotification: $message');

    _saveMessage(message);
  }

  @override
  void onResume(Map<String, dynamic> message) {
    printLog('[app.dart] onResume Pushnotification: $message');

    _saveMessage(message);
  }

  /// Build the App Theme
  ThemeData getTheme(context) {
    printLog("[AppState] build Theme");

    AppModel appModel = Provider.of<AppModel>(context);
    bool isDarkTheme = appModel.darkTheme ?? false;

    if (appModel.appConfig == null) {
      /// This case is loaded first time without config file
      return buildLightTheme(appModel.locale);
    }

    if (isDarkTheme) {
      return buildDarkTheme(appModel.locale).copyWith(
        primaryColor: HexColor(
          appModel.appConfig["Setting"]["MainColor"],
        ),
      );
    }
    return buildLightTheme(appModel.locale).copyWith(
      primaryColor: HexColor(appModel.appConfig["Setting"]["MainColor"]),
    );
  }

  @override
  Widget build(BuildContext context) {
    printLog("[AppState] build");
    return ChangeNotifierProvider<AppModel>.value(
      value: _app,
      child: Consumer<AppModel>(
        builder: (context, value, child) {
          return MultiProvider(
            providers: [
              Provider<ProductModel>.value(value: _product),
              Provider<WishListModel>.value(value: _wishlist),
              Provider<ShippingMethodModel>.value(value: _shippingMethod),
              Provider<PaymentMethodModel>.value(value: _paymentMethod),
              Provider<OrderModel>.value(value: _order),
              Provider<RecentModel>.value(value: _recent),
              Provider<VendorShippingMethodModel>.value(
                  value: _vendorShippingMethod),
              Provider<UserModel>.value(value: _user),
              ChangeNotifierProvider<FilterAttributeModel>(
                  create: (_) => _filterModel),
              ChangeNotifierProvider<FilterTagModel>(
                  create: (_) => _filterTagModel),
              ChangeNotifierProvider<CategoryModel>(
                  create: (_) => _categoryModel),
              ChangeNotifierProvider<StoreModel>(create: (_) => _store),
              ChangeNotifierProvider(create: (_) => cartModel.model),
              ChangeNotifierProvider(create: (_) => BlogModel()),
              ChangeNotifierProvider(create: (_) => _blog),
              ChangeNotifierProvider(create: (_) => _advertisementModel),
            ],
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              locale: Locale(Provider.of<AppModel>(context).locale, ""),
              navigatorObservers: [
                MyRouteObserver(),
                ...firebaseAnalyticsAbs.getMNavigatorObservers()
              ],
              localizationsDelegates: const [
                S.delegate,
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                DefaultCupertinoLocalizations.delegate,
              ],
              supportedLocales: S.delegate.supportedLocales,
              home: Scaffold(
                body: AppInit(
                  onNext: () => MainTabs(),
                ),
              ),
              routes: Routes.getAll(),
              onGenerateRoute: Routes.getRouteGenerate,

              theme: getTheme(context),
            ),
          );
        },
      ),
    );
  }
}
