import 'package:flutter/material.dart';

import '../services/index.dart';

class PaymentSettingsModel extends ChangeNotifier {
  final Services _service = Services();
  PaymentSettings paymentSettings;
  bool isLoading = true;
  String message;
  String cardVaultUrl;

  Future<void> getPaymentSettings() async {
    try {
      paymentSettings = await _service.getPaymentSettings();

      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      message =
          "There is an issue with the app during request the data, please contact admin for fixing the issues " +
              err.toString();
      notifyListeners();
    }
  }

  String getCardVaultUrl() {
    return paymentSettings.cardVaultUrl;
  }
}

class PaymentSettings {
  /// https://shopify.dev/tutorials/create-a-checkout-with-storefront-api
  String vaultId; // apply when checkout credit card

  String cardVaultUrl;
  List<String> acceptedCardBrands;
  String countryCode;
  String currencyCode;

  PaymentSettings(
      {this.cardVaultUrl,
      this.acceptedCardBrands,
      this.countryCode,
      this.currencyCode});

  Map<String, dynamic> toJson() {
    return {
      "cardVaultUrl": cardVaultUrl,
      "acceptedCardBrands": acceptedCardBrands,
      "countryCode": countryCode,
      "enabled": currencyCode
    };
  }
}
