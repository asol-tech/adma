import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

import '../common/constants.dart';
import '../services/config.dart';

class Coupons {
  var coupons = [];

  static Future<Discount> getDiscount({
    Map<String, int> productInCart,
    String couponCode,
  }) async {
    final endpoint = "${Config().url}/wp-json/api/flutter_woo/coupon";
    final lineItems = [];
    for (var id in productInCart.keys) {
      lineItems.add({
        "product_id": id,
        "quantity": productInCart[id],
      });
    }
    final response = await http.post(
      endpoint,
      body: json.encode({
        "line_items": lineItems,
        "coupon_code": "$couponCode",
      }),
    );
    if (response.statusCode == HttpStatus.ok) {
      return Discount.fromJson(json.decode(response.body));
    }
    return null;
  }

  Coupons.getListCoupons(List a) {
    for (var i in a) {
      coupons.add(Coupon.fromJson(i));
    }
  }
}

class Discount {
  Coupon coupon;
  double discount;

  Discount({this.coupon, this.discount});

  Discount.fromJson(Map<String, dynamic> json) {
    coupon = json['coupon'] != null ? Coupon.fromJson(json['coupon']) : null;
    discount = double.parse("${(json['discount'] ?? 0.0)}");
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (coupon != null) {
      data['coupon'] = coupon.toJson();
    }
    data['discount'] = discount;
    return data;
  }
}

class Coupon {
  double amount;
  var code;
  var message;
  var id;
  var discountType;
  var dateExpires;
  var description;
  double minimumAmount;
  double maximumAmount;
  int usageCount;
  bool individualUse;
  List<String> productIds;
  List<String> excludedProductIds;
  int usageLimit;
  int usageLimitPerUser;
  bool freeShipping;
  List<String> productCategories;
  List<String> excludedProductCategories;
  bool excludeSaleItems;
  List<String> emailRestrictions;
  List<String> usedBy;

  bool get isFixedCartDiscount => discountType == "fixed_cart";

  bool get isFixedProductDiscount => discountType == "fixed_product";

  bool get isPercentageDiscount => discountType == "percent";

  Coupon.fromJson(Map<String, dynamic> json) {
    try {
      amount = double.parse(json["amount"].toString());
      code = json["code"];
      id = json["id"];
      discountType = json["discount_type"];
      description = json["description"];
      minimumAmount = json["minimum_amount"] != null
          ? double.parse(json["minimum_amount"].toString())
          : 0.0;
      maximumAmount = json["maximum_amount"] != null
          ? double.parse(json["maximum_amount"].toString())
          : 0.0;
      dateExpires = json["date_expires"] != null
          ? DateTime.parse(json["date_expires"])
          : null;
      message = "";
    } catch (e) {
      printLog(e.toString());
    }
  }

  Coupon.fromShopify(Map<String, dynamic> json) {
    try {
      amount = double.parse(json["totalPrice"].toString());
      code = json["code"];
      id = json["code"];
      discountType = "fixed_cart";
      description = "";
      minimumAmount = 0.0;
      maximumAmount = 0.0;
      dateExpires = null;
      message = "Hello";
    } catch (e) {
      printLog(e.toString());
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "amount": amount,
      "code": code,
      "discount_type": discountType,
      // "description": description,
      // "minimum_amount": minimumAmount,
      // "maximum_amount": maximumAmount,
      // "date_expires": dateExpires,
    };
  }
}
