import '../../common/constants.dart';

import 'user_address.dart';

class User {
  String id;
  bool loggedIn;
  String name;
  String firstName;
  String lastName;
  String username;
  String email;
  String nicename;
  String userUrl;
  String picture;
  String cookie;
  Shipping shipping;
  Billing billing;
  bool isVender;
  bool isSocial = false;

  // from WooCommerce Json
  User.fromWooJson(Map<String, dynamic> json) {
    try {
      var user = json['user'];
      isSocial = true;
      loggedIn = true;
      id = json['wp_user_id'].toString();
      name = user['displayname'];
      cookie = json['cookie'];
      username = user['username'].toString();
      firstName = json['user_login'];
      lastName = '';
      email = user['email'] ?? user['id'];
      isSocial = true;
    } catch (e) {
      printLog(e.toString());
    }
  }
  // from WooCommerce Json
  User.fromWoJson(Map<String, dynamic> json) {
    try {
      var user = json['user'];
      loggedIn = true;
      id = json['wp_user_id'].toString();
      name = json['user_login'];
      cookie = json['cookie'];
      username = user['username'];
      firstName = json['user_login'];
      lastName = '';
      email = user['email'] ?? username;
      isSocial = true;
      var roles = user['role'] as List;
      var role = roles.firstWhere(
          (item) => ((item == 'seller') || (item == 'wcfm_vendor')),
          orElse: () => null);
      if (role != null) {
        isVender = true;
      } else {
        isVender = false;
      }
    } catch (e) {
      printLog(e.toString());
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "loggedIn": loggedIn,
      "name": name,
      "firstName": firstName,
      "lastName": lastName,
      "username": username,
      "email": email,
      "picture": picture,
      "cookie": cookie,
      "nicename": nicename,
      "url": userUrl,
      "isSocial": isSocial,
      "isVender": isVender
    };
  }

  User.fromLocalJson(Map<String, dynamic> json) {
    try {
      loggedIn = json['loggedIn'];
      id = json['id'].toString();
      name = json['name'];
      cookie = json['cookie'];
      username = json['username'];
      firstName = json['firstName'];
      lastName = json['lastName'];
      email = json['email'];
      picture = json['picture'];
      nicename = json['nicename'];
      userUrl = json['url'];
      isSocial = json['isSocial'];
      isVender = json['isVender'];
    } catch (e) {
      printLog(e.toString());
    }
  }

  // from Create User
  User.fromAuthUser(Map<String, dynamic> json, String _cookie) {
    try {
      cookie = _cookie;
      id = json['id'].toString();
      name = json['displayname'];
      username = json['username'];
      firstName = json['firstname'];
      lastName = json['lastname'];
      email = json['email'];
      picture = json['avatar'];
      nicename = json['nicename'];
      userUrl = json['url'];
      loggedIn = true;
      var roles = json['role'] as List;
      if (roles.isNotEmpty) {
        var role = roles.firstWhere(
            (item) => ((item == 'seller') || (item == 'wcfm_vendor')),
            orElse: () => null);
        if (role != null) {
          isVender = true;
        } else {
          isVender = false;
        }
      } else {
        isVender = (json['capabilities']['wcfm_vendor'] as bool) ?? false;
      }
      if (json['shipping'] != null) {
        shipping = Shipping.fromJson(json['shipping']);
      }
      if (json['billing'] != null) {
        billing = Billing.fromJson(json['billing']);
      }
    } catch (e) {
      printLog(e.toString());
    }
  }

  @override
  String toString() => 'User { username: $id $name $email}';
}

class UserPoints {
  int points;
  List<UserEvent> events = [];

  UserPoints.fromJson(Map<String, dynamic> json) {
    points = json['points_balance'];

    if (json['events'] != null) {
      for (var event in json['events']) {
        events.add(UserEvent.fromJson(event));
      }
    }
  }
}

class UserEvent {
  String id;
  String userId;
  String orderId;
  String date;
  String description;
  String points;

  UserEvent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    orderId = json['order_id'];
    date = json['date_display_human'];
    description = json['description'];
    points = json['points'];
  }
}
