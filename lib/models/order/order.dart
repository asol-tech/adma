import '../../common/config.dart';
import '../../common/constants.dart';
import '../address.dart';
import '../cart/cart_model.dart';
import '../product/product.dart';
import '../product/product_variation.dart';

class Order {
  String id;
  String number;
  String status;
  DateTime createdAt;
  DateTime dateModified;
  double total;
  double totalTax;
  String paymentMethodTitle;
  String shippingMethodTitle;
  String customerNote;
  List<ProductItem> lineItems = [];
  Address billing;
  String statusUrl;
  double subtotal;

  Order({this.id, this.number, this.status, this.createdAt, this.total});

  Order.fromJson(Map<String, dynamic> parsedJson) {
    try {
      id = parsedJson["id"].toString();
      customerNote = parsedJson["customer_note"];
      number = parsedJson["number"];
      status = parsedJson["status"];
      createdAt = parsedJson["date_created"] != null
          ? DateTime.parse(parsedJson["date_created"])
          : DateTime.now();
      dateModified = parsedJson["date_modified"] != null
          ? DateTime.parse(parsedJson["date_modified"])
          : DateTime.now();
      total =
          parsedJson["total"] != null ? double.parse(parsedJson["total"]) : 0.0;
      totalTax = parsedJson["total_tax"] != null
          ? double.parse(parsedJson["total_tax"])
          : 0.0;
      paymentMethodTitle = parsedJson["payment_method_title"];

      parsedJson["line_items"].forEach((item) {
        lineItems.add(ProductItem.fromJson(item));
      });

      billing = Address.fromJson(parsedJson["billing"]);
      shippingMethodTitle = parsedJson["shipping_lines"] != null &&
              parsedJson["shipping_lines"].length > 0
          ? parsedJson["shipping_lines"][0]["method_title"]
          : null;
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
  }

  Map<String, dynamic> toOrderJson(CartModel cartModel, userId) {
    var items = lineItems.map((index) {
      return index.toJson();
    }).toList();

    return {
      "status": status,
      "total": total.toString(),
      "shipping_lines": [
        {"method_title": shippingMethodTitle}
      ],
      "number": number,
      "billing": billing,
      "line_items": items,
      "id": id,
      "date_created": createdAt.toString(),
      "payment_method_title": paymentMethodTitle
    };
  }

  Map<String, dynamic> toJson(CartModel cartModel, userId, paid) {
    var lineItems = cartModel.productsInCart.keys.map((key) {
      String productId = Product.cleanProductID(key);
      String productVariantId = ProductVariation.cleanProductVariantID(key);

      var item = {
        "product_id": productId,
        "quantity": cartModel.productsInCart[key]
      };
      if (cartModel.productVariationInCart[key] != null &&
          productVariantId != null) {
        item["variation_id"] = cartModel.productVariationInCart[key].id;
      }

      return item;
    }).toList();

    var params = {
      "set_paid": paid,
      "line_items": lineItems,
      "customer_id": userId,
    };
    try {
      if (cartModel.paymentMethod != null) {
        params["payment_method"] = cartModel.paymentMethod.id;
      }
      if (cartModel.paymentMethod != null) {
        params["payment_method_title"] = cartModel.paymentMethod.title;
      }
      if (paid) params["status"] = "processing";

      if (cartModel.address != null &&
          cartModel.address.mapUrl != null &&
          cartModel.address.mapUrl.isNotEmpty) {
        params["customer_note"] = "URL:" + cartModel.address.mapUrl;
      }
      if (kPaymentConfig['EnableReview'] &&
          cartModel.notes != null &&
          cartModel.notes.isNotEmpty) {
        if (params["customer_note"] != null) {
          params["customer_note"] += "\n" + cartModel.notes;
        } else {
          params["customer_note"] = cartModel.notes;
        }
      }

      if (kPaymentConfig['EnableAddress'] && cartModel.address != null) {
        params["billing"] = cartModel.address.toJson();
        params["shipping"] = cartModel.address.toJson();
      }

      if (kPaymentConfig['EnableShipping'] &&
          cartModel.selectedShippingMethods.isNotEmpty) {
        List<Map<String, dynamic>> shippings = [];
        cartModel.selectedShippingMethods.forEach((element) {
          shippings.add({
            "method_id":
                "${element.shippingMethods[0].methodId}:${element.shippingMethods[0].id}",
            "method_title": element.shippingMethods[0].title,
          });
        });
        params["shipping_lines"] = shippings;
      }

      if (cartModel.couponObj != null) {
        params["coupon_lines"] = [
          cartModel.couponObj.toJson(),
        ];
        params["subtotal"] = cartModel.getSubTotal();
        params["total"] = cartModel.getTotal();
      }
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
    return params;
  }

  @override
  String toString() => 'Order { id: $id  number: $number}';
}

class ProductItem {
  String productId;
  String name;
  int quantity;
  String total;

  ProductItem.fromJson(Map<String, dynamic> parsedJson) {
    try {
      productId = parsedJson["product_id"].toString();
      name = parsedJson["name"];
      quantity = int.parse("${parsedJson["quantity"]}");
      total = parsedJson["total"];
    } catch (e, trace) {
      printLog(e.toString());
      printLog(trace.toString());
    }
  }

  Map<String, dynamic> toJson() {
    return {
      "product_id": productId,
      "name": name,
      "quantity": quantity,
      "total": total
    };
  }
}
