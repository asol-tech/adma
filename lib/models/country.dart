import 'package:country_pickers/country_pickers.dart';
import 'package:localstorage/localstorage.dart';

class State {
  String id;
  String code;
  String name;
  State({this.id, this.code, this.name});

  State.fromConfig(dynamic parsedJson) {
    if (parsedJson is Map) {
      id = parsedJson["code"];
      code = parsedJson["code"];
      name = parsedJson["name"];
    }
    if (parsedJson is String) {
      id = parsedJson;
      code = parsedJson;
      name = parsedJson;
    }
  }

  State.fromMagentoJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  State.fromOpencartJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["zone_id"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }

  State.fromPrestashop(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"].toString();
    name = parsedJson["name"];
    code = parsedJson["iso_code"];
  }

  State.fromWooJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["code"];
    code = parsedJson["code"];
    name = parsedJson["name"];
  }
}

class Country {
  String id;
  String code;
  String name;
  String icon;
  String id_country;
  List<State> states = [];

  Country({this.id, this.name, this.states});

  Country.fromConfig(this.id, this.name, this.icon, List states) {
    code = id;
    name =
        name != null ? name : CountryPickerUtils.getCountryByIsoCode(id).name;
    if (states != null) {
      for (var item in states) {
        states.add(State.fromConfig(item));
      }
    }
  }

  Country.fromWooJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["code"];
    code = parsedJson["code"];
    name = parsedJson["name"];
    states = [];
  }

  Map<String, dynamic> toJson() {
    return {"id": id, "name": name, "states": states};
  }

  Country.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'];
      code = json['code'];
      name = json['name'];
      states = json['states'];
    } catch (_) {}
  }

  Future<void> saveToLocal() async {
    final LocalStorage storage = LocalStorage("address");
    try {
      final ready = await storage.ready;
      if (ready) {
        await storage.setItem('', toJson());
      }
    } catch (_) {}
  }
}

class ListCountry {
  List<Country> list = [];

  ListCountry({this.list});

  ListCountry.fromWooJson(List json) {
    if (json != null && json is List) {
      for (var item in json) {
        list.add(Country.fromWooJson(item));
      }
    }
  }
}
