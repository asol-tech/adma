import 'product_attribute.dart';

class ProductVariation {
  String id;
  String sku;
  String price;
  String regularPrice;
  String salePrice;
  bool onSale;
  bool inStock;
  int stockQuantity;
  String imageFeature;
  List<Attribute> attributes = [];

  ProductVariation();

  ProductVariation.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"].toString();
    price = parsedJson["price"].toString();
    regularPrice = parsedJson["regular_price"].toString();
    salePrice = parsedJson["sale_price"].toString();
    onSale = parsedJson["on_sale"];
    inStock = parsedJson["in_stock"];
    inStock ? stockQuantity = parsedJson["stock_quantity"] : stockQuantity = 0;
    imageFeature = parsedJson["image"]["src"];

    List<Attribute> attributeList = [];
    parsedJson["attributes"].forEach((item) {
      attributeList.add(Attribute.fromJson(item));
    });
    attributes = attributeList;
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "sku": sku,
      "price": price,
      "regularPrice": regularPrice,
      "sale_price": salePrice,
      "on_sale": onSale,
      "in_stock": inStock,
      "stock_quantity": stockQuantity,
      "image": {"src": imageFeature},
      "attributes": attributes.map((item) {
        return item.toJson();
      }).toList()
    };
  }

  ProductVariation.fromLocalJson(Map<String, dynamic> json) {
    try {
      id = json['id'];
      sku = json["sku"];
      price = json['price'];
      regularPrice = json['regularPrice'];
      onSale = json['onSale'];
      salePrice = json['salePrice'];
      inStock = json['inStock'];
      inStock ? stockQuantity = json["stock_quantity"] : stockQuantity = 0;
      imageFeature = json['image']["src"];
      List<Attribute> attributeList = [];

      if (json['attributes'] != null) {
        for (var item in json['attributes']) {
          attributeList.add(Attribute.fromLocalJson(item));
        }
      }

      attributes = attributeList;
    } catch (_) {}
  }

  ProductVariation.fromPrestaJson(Map<String, dynamic> json) {
    id = json["id"].toString();
    regularPrice =
        (double.parse((json["price"] ?? 0.0).toString())).toStringAsFixed(2);
    salePrice = (double.parse((json["wholesale_price"] ?? 0.0).toString()))
        .toStringAsFixed(2);
    price = (double.parse((json["wholesale_price"] ?? 0.0).toString()))
        .toStringAsFixed(2);
    if (salePrice != regularPrice) {
      onSale = true;
    } else {
      onSale = false;
    }
    stockQuantity =
        json["quantity"].isNotEmpty ? int.parse(json["quantity"]) : 0;
    if (stockQuantity > 0) {
      inStock = true;
    } else {
      inStock = false;
    }
  }

  /// Get product ID from mix String productID-ProductVariantID
  static String cleanProductVariantID(productString) {
    return productString.contains('-') ? productString.split('-')[1] : null;
  }
}
