class CheckoutCart {
  dynamic id;
  String webUrl;
  double subtotalPrice;
  double totalTax;
  double totalPrice;
  double paymentDue;

  @override
  String toString() => 'Checkout { id: $id }';
}
