import 'cart_base.dart';
import 'cart_model_woo.dart';

export 'cart_base.dart';

class CartInject {
  static final CartInject _instance = CartInject._internal();

  factory CartInject() => _instance;

  CartInject._internal();

  CartModel model;

  void init(config) {
    switch (config['type']) {
      default:
        model = CartModelWoo();
    }
    model.initData();
  }
}
