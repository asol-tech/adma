import 'cart_mixin.dart';
import '../../../vendor/checkout/vendor_shipping_model.dart';

mixin VendorMixin on CartMixin {
  List<VendorShippingMethod> selectedShippingMethods = [];

  double getShippingCost() {
    double sum = 0.0;
    selectedShippingMethods.forEach((element) {
      sum += element.shippingMethods[0].cost ?? 0.0;
    });
    return sum;
  }

  void setSelectedMethods(List<VendorShippingMethod> selected) {
    selectedShippingMethods = selected;
  }
}
