import 'dart:convert' as convert;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../common/config.dart';
import '../common/constants.dart';
import '../services/index.dart';
import 'cart/cart_model.dart';
import 'category/category_model.dart';

class AppConstants {
  static const String featureStory = 'Story';
}

class AppModel with ChangeNotifier {
  Map<String, dynamic> appConfig;
  Map<String, dynamic> featuresConfig = Map();
  bool isLoading = true;
  String message;
  bool darkTheme = false;
  String locale = kAdvanceConfig['DefaultLanguage'];
  List<String> categories;
  String productListLayout;
  List<int> ratioProduct;
  String currency; //USD, VND
  Map<String, dynamic> currencyRate = Map<String, dynamic>();
  bool showDemo = false;
  String username;
  bool isInit = false;
  Map<String, dynamic> drawer;
  Map deeplink;

  bool get avaliableStory =>
      featuresConfig?.containsKey(AppConstants.featureStory) ?? false;

  AppModel() {
    getConfig();
  }

  Future<bool> getConfig() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      locale = prefs.getString("language") ?? kAdvanceConfig['DefaultLanguage'];
      darkTheme = prefs.getBool("darkTheme") ?? false;
      currency = prefs.getString("currency") ??
          (kAdvanceConfig['DefaultCurrency'] as Map)['currency'];
      isInit = true;
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<bool> changeLanguage(String country, BuildContext context) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      locale = country;

      await prefs.setString("language", country);
      await loadAppConfig(isSwitched: true);

//      notifyListeners();

      await Provider.of<CategoryModel>(context, listen: false)
          .getCategories(lang: country, cats: categories);
      return true;
    } catch (err) {
      return false;
    }
  }

  Future<void> changeCurrency(String item, BuildContext context) async {
    try {
      Provider.of<CartModel>(context, listen: false).changeCurrency(item);
      SharedPreferences prefs = await SharedPreferences.getInstance();
      currency = item;
      await prefs.setString("currency", currency);
      notifyListeners();
    } catch (error) {
      printLog('[_getFacebookLink] error: ${error.toString()}');
    }
  }

  Future<void> updateTheme(bool theme) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      darkTheme = theme;
      await prefs.setBool("darkTheme", theme);
      notifyListeners();
    } catch (error) {
      printLog('[_getFacebookLink] error: ${error.toString()}');
    }
  }

  void updateShowDemo(bool value) {
    showDemo = value;
    notifyListeners();
  }

  void updateUsername(String user) {
    username = user;
    notifyListeners();
  }

  void loadStreamConfig(config) {
    appConfig = config;
    productListLayout = appConfig['Setting']['ProductListLayout'];
    isLoading = false;
    notifyListeners();
  }

  Future<Map> loadAppConfig({isSwitched = false}) async {
    try {
      if (!isInit) {
        await getConfig();
      }
      final LocalStorage storage = LocalStorage('builder.json');
      var config = await storage.getItem('config');
      if (config != null) {
        appConfig = config;
      } else {
        /// we only apply the http config if isUpdated = false, not using switching language
        // ignore: prefer_contains
        if (kAppConfig.indexOf('http') != -1) {
          // load on cloud config and update on air
          String path = kAppConfig;
          if (path.contains('.json')) {
            path = path.substring(0, path.lastIndexOf('/'));
            path += '/config_$locale.json';
          }
          final appJson = await http.get(Uri.encodeFull(path),
              headers: {"Accept": "application/json"});
          appConfig =
              convert.jsonDecode(convert.utf8.decode(appJson.bodyBytes));
        } else {
          // load local config
          String path = "lib/config/config_$locale.json";
          try {
            final appJson = await rootBundle.loadString(path);
            appConfig = convert.jsonDecode(appJson);
          } catch (e) {
            final appJson = await rootBundle.loadString(kAppConfig);
            appConfig = convert.jsonDecode(appJson);
          }
        }
      }
      productListLayout = appConfig['Setting']['ProductListLayout'];
      final ratioImageProduct = appConfig['Setting']['ratioImageProduct'];
      final ratioInfoProduct = appConfig['Setting']['ratioInfoProduct'];
      if (ratioImageProduct != null && ratioInfoProduct != null) {
        ratioProduct = [ratioImageProduct, ratioInfoProduct];
      }

      drawer = appConfig['Drawer'] != null
          ? Map<String, dynamic>.from(appConfig['Drawer'])
          : null;

      var categoryTab = appConfig['TabBar']
          .firstWhere((e) => e['layout'] == 'category', orElse: () => {});

      if (categoryTab['categories'] != null) {
        categories = List<String>.from(categoryTab['categories']);
      }

      /// apply App Caching if isCaching is enable
      /// we only load this setting at first time login, not from switching languages
      if (!isSwitched) {
        await Services().widget.onLoadedAppConfig((configCache) {
          appConfig = configCache;
        });
      }

      /// Load the Rate for Product Currency
      final rates = await Services().getCurrencyRate();
      if (rates != null) {
        currencyRate = rates;
      }
      isLoading = false;

      printLog('[Debug] Finish Load AppConfig');

      notifyListeners();

      return appConfig;
    } catch (err, trace) {
      printLog(trace);
      isLoading = false;
      message = err.toString();
      notifyListeners();
      return null;
    }
  }

  Future<Map> loadFeature(String url) async {
    if (url.contains('http')) {
      final _reuslt = await http
          .get(Uri.encodeFull(url), headers: {"Accept": "application/json"});
      return convert.jsonDecode(convert.utf8.decode(_reuslt.bodyBytes));
    } else {
      try {
        final _result = await rootBundle.loadString(url);
        return convert.jsonDecode(_result);
      } catch (e) {
        return null;
      }
    }
  }

  void updateProductListLayout(layout) {
    productListLayout = layout;
    notifyListeners();
  }
}

class App {
  Map<String, dynamic> appConfig;

  App(this.appConfig);
}
