class Review {
  int id;
  int productId;
  String name;
  String email;
  String review;
  double rating;
  DateTime createdAt;
  String avatar;

  Review.fromJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = parsedJson["name"];
    email = parsedJson["email"];
    review = parsedJson["review"];
    rating = double.parse(parsedJson["rating"].toString());
    createdAt = parsedJson["date_created"] != null
        ? DateTime.parse(parsedJson["date_created"])
        : DateTime.now();
  }

  Review.fromWCFMJson(Map<String, dynamic> parsedJson) {
    id = int.parse(parsedJson["ID"]);
    name = parsedJson["author_name"];
    email = parsedJson["author_email"];
    review = parsedJson["review_description"];
    avatar = parsedJson["author_image"];
    rating = double.parse(parsedJson["review_rating"]);
    createdAt = parsedJson["created"] != null
        ? DateTime.parse(parsedJson["created"])
        : DateTime.now();
  }

  Review.fromDokanJson(Map<String, dynamic> parsedJson) {
    id = parsedJson["id"];
    name = parsedJson["author"]["name"];
    email = parsedJson["author"]["email"];
    avatar = parsedJson["author"]["avatar"];
    review = parsedJson["content"];
    rating = double.parse("${parsedJson["rating"]}");
    createdAt = parsedJson["date"] != null
        ? DateTime.parse(parsedJson["date"])
        : DateTime.now();
  }

  @override
  String toString() => 'Category { id: $id  name: $name}';
}
