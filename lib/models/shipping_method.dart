import 'package:flutter/material.dart';

import '../common/constants.dart';
import '../models/cart/cart_model.dart';
import '../services/index.dart';

class ShippingMethodModel extends ChangeNotifier {
  final Services _service = Services();
  List<ShippingMethod> shippingMethods;
  bool isLoading = true;
  String message;

  Future<void> getShippingMethods(
      {CartModel cartModel, String token, String checkoutId}) async {
    try {
      isLoading = true;
      notifyListeners();
      shippingMethods = await _service.getShippingMethods(
          cartModel: cartModel, token: token, checkoutId: checkoutId);
      isLoading = false;
      message = null;
      notifyListeners();
    } catch (err) {
      isLoading = false;
      message = "⚠️ " + err.toString();
      notifyListeners();
    }
  }
}

class ShippingMethod {
  String id;
  String title;
  String description;
  double cost;
  double min_amount;
  String classCost;
  String methodId;
  String methodTitle;

  Map<String, dynamic> toJson() {
    return {"id": id, "title": title, "description": description, "cost": cost};
  }

  ShippingMethod.fromJson(Map<String, dynamic> parsedJson) {
    try {
      id = "${parsedJson["instance_id"]}";
      title = parsedJson["label"];
      //description = parsedJson["method_description"];
      methodId = parsedJson["method_id"];
      methodTitle = parsedJson["label"];
      cost = double.parse(parsedJson["cost"]);
//      if (parsedJson["settings"]["cost"] != null) {
//        cost = double.parse(parsedJson["settings"]["cost"]["value"]);
//      }
//      parsedJson["settings"]["min_amount"] != null
//          ? min_amount =
//              double.parse(parsedJson["settings"]["min_amount"]["value"])
//          : min_amount = null;
//      Map settings = parsedJson["settings"];
//      settings.keys.forEach((key) {
//        if (key is String && key.contains("class_cost_")) {
//          classCost = parsedJson["settings"][key]["value"];
//        }
//      });
    } catch (e) {
      printLog('error parsing Shipping method');
    }
  }
}
