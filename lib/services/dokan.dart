import 'dart:async';
import 'dart:convert' as convert;
import "dart:core";

import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';

import '../vendor/store/store_model.dart';
import '../models/product/product.dart';
import '../models/review.dart';
import '../models/user/user.dart';
import 'helper/dokan_api.dart';
import 'woo_commerce.dart';
import '../models/order/order_model.dart';
import '../models/cart/cart_model.dart';
import '../models/shipping_method.dart';

class DokanApi with WooCommerce {
  static final DokanApi _instance = DokanApi._internal();

  factory DokanApi() => _instance;

  DokanApi._internal();

  DokanAPI dokanApi;

  @override
  void appConfig(appConfig) {
    super.appConfig(appConfig);
    dokanApi = DokanAPI(url: appConfig["url"]);
  }

  @override
  Future<User> createUser(
      {firstName, lastName, username, password, isVender = false}) async {
    try {
      String niceName = firstName + " " + lastName;
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_user/sign_up/?insecure=cool&$isSecure",
          body: convert.jsonEncode({
            "user_email": username,
            "user_login": username,
            "username": username,
            "first_name": firstName,
            "last_name": lastName,
            "user_pass": password,
            "email": username,
            "user_nicename": niceName,
            "display_name": niceName,
            "role": isVender ? "seller" : "subscriber"
          }));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body["message"] == null) {
        var cookie = body['cookie'];
        return await getUserInfo(cookie);
      } else {
        var message = body["message"];
        throw Exception(message != null ? message : "Can not create the user.");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Store>> getStores({lang, page}) async {
    try {
      List<Store> list = [];
      var response = await dokanApi.getAsync("stores?page=$page&per_page=10");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Store.fromDokanJson(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Store>> getFeaturedStores() async {
    int page = 1;
    List<Store> list = [];
    while (true) {
      try {
        var response =
            await dokanApi.getAsync("stores?page=$page&per_page=100");
        if (response.length == 0) {
          return list;
        }
        if (response is Map && isNotBlank(response["message"])) {
          throw Exception(response["message"]);
        } else {
          for (var item in response) {
            if (item['featured']) list.add(Store.fromDokanJson(item));
          }
          page++;
        }
      } catch (e) {
        return list;
      }
    }
  }

  @override
  Future<List<Product>> getProductsByStore({storeId, page}) async {
    try {
      List<Product> list = [];
      var response = await dokanApi
          .getAsync("stores/$storeId/products?page=$page&per_page=10");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          final product = Product.fromJson(item);
          product.store = Store.fromDokanJson(item["store"]);
          list.add(product);
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviewsStore({storeId}) async {
    try {
      List<Review> list = [];
      var response = await dokanApi.getAsync("stores/$storeId/reviews");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Review.fromDokanJson(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Store> getStoreInfo(storeId) async {
    try {
      var response = await dokanApi.getAsync("stores/$storeId");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        return Store.fromDokanJson(response);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future getJWTToken() {
    // TODO: implement getJWTToken
    return null;
  }

  @override
  Future<List<ShippingMethod>> getVendorShippingMethods(
      CartModel cartModel, String vendorId) async {
    try {
      Map<String, dynamic> params = Order().toJson(cartModel, null, false);
      if (vendorId != "-1") {
        params["seller_id"] = vendorId;
      }
      List<ShippingMethod> list = [];
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_woo/shipping_methods",
          body: convert.jsonEncode(params));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        for (var item in body) {
          list.add(ShippingMethod.fromJson(item));
        }
      } else if (body["message"] != null) {
        throw Exception(body["message"]);
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Store>> searchStores({String keyword, int page}) async {
    try {
      List<Store> list = [];
      var endPoint = 'stores?';
      if (keyword?.isNotEmpty ?? false) {
        endPoint += 'search=$keyword';
      }

      endPoint += '&page=$page';

      var response = await dokanApi.getAsync(endPoint);

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          list.add(Store.fromDokanJson(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }
}
