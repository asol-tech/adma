import 'dart:async';
import 'dart:convert' as convert;
import "dart:core";

import 'package:http/http.dart' as http;
import 'package:quiver/strings.dart';

import '../models/order/order_model.dart';
import '../models/cart/cart_model.dart';
import '../models/product/product.dart';
import '../models/review.dart';
import '../models/shipping_method.dart';
import '../models/user/user.dart';
import '../vendor/store/store_model.dart';
import 'helper/wcfm_api.dart';
import 'woo_commerce.dart';

class WCFMApi with WooCommerce {
  static final WCFMApi _instance = WCFMApi._internal();

  factory WCFMApi() => _instance;

  WCFMApi._internal();

  WCFMAPI wcfmApi;

  String jwtToken;

  @override
  void appConfig(appConfig) {
    super.appConfig(appConfig);
    wcfmApi = WCFMAPI(url: appConfig["url"]);
  }

  @override
  Future<User> createUser(
      {firstName, lastName, username, password, isVender = false}) async {
    try {
      String niceName = firstName + " " + lastName;
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_user/sign_up/?insecure=cool&$isSecure",
          body: convert.jsonEncode({
            "user_email": username,
            "user_login": username,
            "username": username,
            "first_name": firstName,
            "last_name": lastName,
            "user_pass": password,
            "email": username,
            "user_nicename": niceName,
            "display_name": niceName,
            "role": isVender ? "wcfm_vendor" : "subscriber"
          }));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200 && body["message"] == null) {
        var cookie = body['cookie'];
        return await getUserInfo(cookie);
      } else {
        var message = body["message"];
        throw Exception(message != null ? message : "Can not create the user.");
      }
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Store>> getStores({lang, page}) async {
    try {
      List<Store> list = [];
      var response =
          await wcApi.getAsync("flutter/wcfm-stores?per_page=10&page=$page");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          if (!item["disable_vendor"]) list.add(Store.fromWCFMJson(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Product>> getProductsByStore({storeId, page}) async {
    try {
      final response = await wcApi
          .postAsync('flutter/products/owner', {"id": storeId, "page": page});
      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        List<Product> list = [];
        for (var item in response) {
          if (item["status"] == "published" || item["status"] == "publish") {
            final product = Product.fromJson(item);
            if (item['store'] != null) {
              product.store = Store.fromWCFMJson(item["store"]);
            }
            list.add(product);
          }
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<Review>> getReviewsStore({storeId}) async {
    try {
      List<Review> list = [];
      var response = await wcfmApi.getAsync("reviews");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          if (int.parse(item["vendor_id"]) == storeId) {
            list.add(Review.fromWCFMJson(item));
          }
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<Store> getStoreInfo(storeId) async {
    try {
      var response = await wcApi.getAsync("flutter/wcfm-stores/$storeId");

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        return Store.fromWCFMJson(response);
      }
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<ShippingMethod>> getVendorShippingMethods(
      CartModel cartModel, String vendorId) async {
    try {
      Map<String, dynamic> params = Order().toJson(cartModel, null, false);
      if (vendorId != "-1") {
        params["seller_id"] = vendorId;
      }
      List<ShippingMethod> list = [];
      final http.Response response = await http.post(
          "$url/wp-json/api/flutter_woo/shipping_methods",
          body: convert.jsonEncode(params));
      final body = convert.jsonDecode(response.body);
      if (response.statusCode == 200) {
        for (var item in body) {
          list.add(ShippingMethod.fromJson(item));
        }
      } else if (body["message"] != null) {
        throw Exception(body["message"]);
      }
      return list;
    } catch (err) {
      rethrow;
    }
  }

  @override
  Future<List<Store>> searchStores({String keyword, int page = 10}) async {
    try {
      List<Store> list = [];
      var endPoint = 'flutter/wcfm-stores?';
      if (keyword?.isNotEmpty ?? false) {
        endPoint += 'search=$keyword';
      }

      endPoint += '&page=$page';

      var response = await wcApi.getAsync(endPoint);

      if (response is Map && isNotBlank(response["message"])) {
        throw Exception(response["message"]);
      } else {
        for (var item in response) {
          if (!item["disable_vendor"]) list.add(Store.fromWCFMJson(item));
        }
        return list;
      }
    } catch (e) {
      rethrow;
    }
  }
}
