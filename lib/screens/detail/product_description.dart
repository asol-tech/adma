import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../generated/l10n.dart';
import '../../models/product/product.dart';
import '../../services/index.dart';
import '../../widgets/common/expansion_info.dart';
import 'additional_information.dart';
import 'review.dart';

class ProductDescription extends StatelessWidget {
  final Product product;

  ProductDescription(this.product);

  bool get enableBrand => kProductDetail['enableBrand'] ?? false;

  String get brand {
    final brands = product.infors.where((element) => element.name == 'Brand');
    if (brands?.isEmpty ?? true) return 'Unknown';
    return brands.first.options?.first ?? 'Unknown';
  }

  @override
  Widget build(BuildContext context) {
    bool enableReview =
        Services().widget.enableProductReview && kProductDetail['enableReview'];

    return Column(
      children: <Widget>[
        const SizedBox(height: 15),
        if (product.description != null)
          ExpansionInfo(
            title: S.of(context).description,
            children: <Widget>[
              HtmlWidget(
                product.description.replaceAll('src="//', 'src="https://'),
                webView: true,
                textStyle: TextStyle(color: Theme.of(context).accentColor),
              ),
            ],
            expand: true,
          ),
        if (enableReview) ...[
          Container(
            height: 1,
            decoration: const BoxDecoration(color: kGrey200),
          ),
          ExpansionInfo(
            title: S.of(context).readReviews,
            children: <Widget>[
              Reviews(product.id),
            ],
          ),
        ],
        if (enableBrand) ...[
          Spacer,
          buildBrand(),
        ],
        Container(height: 1, decoration: const BoxDecoration(color: kGrey200)),
        if (product.infors?.isNotEmpty ?? false)
          ExpansionInfo(
            title: S.of(context).additionalInformation,
            children: <Widget>[
              AdditionalInformation(
                listInfo: product.infors,
              ),
            ],
          ),
      ],
    );
  }

  Widget get Spacer => Container(
        height: 1,
        decoration: const BoxDecoration(color: kGrey200),
      );

  Widget buildBrand() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text(
            'Brand',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
          ),
          Text(
            brand ?? 'Unknown',
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}
