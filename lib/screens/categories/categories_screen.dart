import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../vendor/store/store_screen.dart';
import '../../common/constants.dart';
import '../../generated/l10n.dart';
import '../../models/app.dart';
import '../../models/category/category.dart';
import '../../models/category/category_model.dart';
import '../../widgets/cardlist/index.dart';
import '../custom/smartchat.dart';
import 'card.dart';
import 'column.dart';
import 'grid_category.dart';
import 'side_menu.dart';
import 'sub.dart';

class CategoriesScreen extends StatefulWidget {
  final String layout;
  final List<dynamic> categories;
  final List<dynamic> images;
  final bool showChat;

  CategoriesScreen(
      {Key key, this.layout, this.categories, this.images, this.showChat})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CategoriesScreenState();
  }
}

class CategoriesScreenState extends State<CategoriesScreen>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  final FocusNode _focus = FocusNode();
  String searchText;
  var textController = TextEditingController();

  Animation<double> animation;
  AnimationController controller;
  bool _isSelectedCategoriesTab = false;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    animation = Tween<double>(begin: 0, end: 60).animate(controller);
    animation.addListener(() {
      setState(() {});
    });

    _focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (_focus.hasFocus && animation.value == 0) {
      controller.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final category = Provider.of<CategoryModel>(context);
    final bool showChat = widget.showChat ?? false;

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight + 8),
        child: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
          elevation: 0,
          centerTitle: true,
          title: FittedBox(
            fit: BoxFit.cover,
            child: Container(
              width: 250,
              child: CupertinoSlidingSegmentedControl<int>(
                backgroundColor: Theme.of(context).primaryColorLight,
                thumbColor: Theme.of(context).backgroundColor,
                children: {
                  0: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      S.of(context).stores.toUpperCase(),
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  1: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      S.of(context).categories.toUpperCase(),
                      style: const TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                  ),
                },
                onValueChanged: (value) {
                  setState(() {
                    _isSelectedCategoriesTab = value == 1;
                  });
                },
                groupValue: _isSelectedCategoriesTab ? 1 : 0,
//                  isSelected: [
//                    _isSelectedCategoriesTab,
//                    !_isSelectedCategoriesTab
//                  ],
              ),
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: GestureDetector(
                onTap: () => Navigator.of(context).pushNamed(RouteList.map),
                child: Padding(
                  padding: const EdgeInsets.only(right: 6.0),
                  child: Icon(
                    Icons.place,
                    size: 26,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      floatingActionButton: showChat
          ? SmartChat(
              margin: EdgeInsets.only(
                right:
                    Provider.of<AppModel>(context, listen: false).locale == 'ar'
                        ? 30.0
                        : 0.0,
              ),
            )
          : Container(),
      body: ListenableProvider.value(
        value: category,
        child: Consumer<CategoryModel>(builder: (context, value, child) {
          if (value.isLoading) {
            return kLoadingWidget(context);
          }

          if (value.categories == null) {
            return Container(
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.center,
              child: Text(S.of(context).dataEmpty),
            );
          }

          List<Category> categories = value.categories;

          return SafeArea(
            child: ['grid', 'column', 'sideMenu', 'subCategories']
                    .contains(widget.layout)
                ? Column(
                    children: <Widget>[
                      Expanded(
                        child: _isSelectedCategoriesTab
                            ? renderCategories(categories)
                            : StoreScreen(),
                      )
                    ],
                  )
                : !_isSelectedCategoriesTab
                    ? StoreScreen()
                    : ListView(
                        children: <Widget>[renderCategories(categories)],
                      ),
          );
        }),
      ),
    );
  }

  Widget renderCategories(value) {
    switch (widget.layout) {
      case 'card':
        return CardCategories(value);
      case 'column':
        return ColumnCategories(value);
      case 'subCategories':
        return SubCategories(value);
      case 'sideMenu':
        return SideMenuCategories(value);
      case 'animation':
        return HorizonMenu(value);
      case 'grid':
        return GridCategory(
          value,
          icons: widget.images,
        );
      default:
        return HorizonMenu(value);
    }
  }

  @override
  void dispose() {
    controller.dispose();
    _focus.dispose();
    super.dispose();
  }
}
