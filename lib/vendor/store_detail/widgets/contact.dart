import 'package:flutter/material.dart';
import 'package:quiver/strings.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../store/store_model.dart';
import 'store_map.dart';

class Contact extends StatelessWidget {
  final Store store;

  Contact({this.store});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      margin: const EdgeInsets.symmetric(vertical: 18.0),
      child: Column(
        children: <Widget>[
          if (isNotBlank(store.address))
            InfoItem(
              icon: const Icon(
                Icons.location_on,
                size: 20,
              ),
              title: store.address,
            ),
          if (isNotBlank(store.phone))
            InfoItem(
              icon: const Icon(
                Icons.phone,
                size: 20,
              ),
              title: store.phone,
              onTap: () async {
                final url = 'tel:' + store.phone;
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
          if (isNotBlank(store.email))
            InfoItem(
              icon: const Icon(
                Icons.email,
                size: 20,
              ),
              title: store.email,
              onTap: () async {
                final url = 'mailto:' + store.email;
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
          if (isNotBlank(store.website))
            InfoItem(
              icon: const Icon(
                Icons.web,
                size: 20,
              ),
              title: store.website,
              onTap: () async {
                final url = store.website;
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
            ),
          if (store.lat != null && store.long != null) StoreMap(store: store),
        ],
      ),
    );
  }
}

class InfoItem extends StatelessWidget {
  final Icon icon;
  final String title;
  final VoidCallback onTap;

  InfoItem({this.icon, this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 2),
            child: icon,
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: InkWell(
              onTap: onTap,
              child: Text(
                title,
                style: Theme.of(context).primaryTextTheme.subtitle1.copyWith(
                    color: onTap != null
                        ? Colors.blue
                        : Theme.of(context).accentColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
