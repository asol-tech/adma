import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:quiver/strings.dart';

import '../../../common/constants.dart';
import '../../../common/tools.dart';
import '../../../generated/l10n.dart';
import '../../../widgets/common/start_rating.dart';
import '../../store_detail/store_detail_screen.dart';
import '../store_model.dart';

class StoreItem extends StatelessWidget {
  final Store store;

  StoreItem({this.store});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, RouteList.storeDetail,
            arguments: StoreDetailArgument(store: store));
      },
      child: Container(
        margin: const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 20.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.0),
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              const BoxShadow(
                color: Colors.black12,
                offset: Offset(0, 1),
                blurRadius: 20,
              )
            ]),
        child: Column(
          children: [
            if (isNotBlank(store.banner))
              ClipRRect(
                borderRadius: BorderRadius.circular(6.0),
                child: Tools.image(
                  url: store.banner,
                  size: kSize.medium,
                  isResize: false,
                  fit: BoxFit.fitWidth,
                  height: 120,
                ),
              ),
            if (!isNotBlank(store.banner))
              Image.asset(kDefaultStoreImage, height: 50),
            Container(
              padding: const EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(3.0),
                color: Theme.of(context).backgroundColor,
              ),
              child: Row(
                children: <Widget>[
                  if (isNotBlank(store.banner))
                    ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: ExtendedImage.network(
                        store.banner,
                        width: 40,
                        height: 40,
                        fit: BoxFit.cover,
                        cache: true,
                        enableLoadState: false,
                      ),
                    ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          store.name,
                          style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600,
                              color: Theme.of(context).accentColor),
                        ),
                        if (isNotBlank(store.address))
                          const SizedBox(height: 3),
                        if (isNotBlank(store.address))
                          Text(
                            store.address,
                            maxLines: 1,
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Theme.of(context).accentColor),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      SmoothStarRating(
                          allowHalfRating: true,
                          starCount: 5,
                          rating: store.rating ?? 0.0,
                          size: 15.0,
                          color: theme.primaryColor,
                          borderColor: theme.primaryColor,
                          label: const Text(''),
                          spacing: 0.0),
                      const SizedBox(height: 10),
                      Text(
                        S.of(context).visitStore,
                        style: Theme.of(context)
                            .primaryTextTheme
                            .subtitle1
                            .copyWith(color: Colors.blue, fontSize: 12),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
