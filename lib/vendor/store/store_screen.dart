import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../screens/search/widgets/search_box.dart';
import '../../common/constants.dart';
import 'store_model.dart';
import 'widgets/store_item.dart';

class StoreScreen extends StatefulWidget {
  @override
  _StoresState createState() => _StoresState();
}

class _StoresState extends State<StoreScreen>
    with AfterLayoutMixin<StoreScreen> {
  RefreshController _refreshController;
  String _currentName = '';

  StoreModel storeModel;

  @override
  initState() {
    super.initState();
    _refreshController = RefreshController(initialRefresh: false);
    storeModel = Provider.of<StoreModel>(context, listen: false);
  }

  @override
  void afterFirstLayout(BuildContext context) {
    storeModel.loadStore();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 2),
          child: SearchBox(
            onChanged: (val) {
              _currentName = val;
              storeModel.loadStore(name: _currentName);
            },
          ),
        ),
        Expanded(
          child: Consumer<StoreModel>(builder: (context, model, child) {
            if (model.stores == null) {
              return Container(
                height: MediaQuery.of(context).size.height - 150,
                child: Center(
                  child: kLoadingWidget(context),
                ),
              );
            }

            if (model.stores.isEmpty) {
              return Container();
            }

            return SmartRefresher(
              enablePullDown: false,
              enablePullUp: !model.isEnd,
              controller: _refreshController,
              onLoading: () {
                model.loadStore(
                    name: _currentName,
                    onFinish: () {
                      _refreshController.loadComplete();
                    });
              },
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(vertical: 12),
                itemCount: model.stores.length,
                itemBuilder: (_, index) => StoreItem(
                  store: model.stores[index],
                ),
              ),
            );
          }),
        ),
      ],
    );
  }
}
