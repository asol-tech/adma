import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';

import '../../vendor/store/store_model.dart';
import '../../widgets/common/skeleton.dart';
import 'map_screen_model.dart';
import 'store_banner.dart';

const kZoomMap = 16.0;

/// Map Screen
class MapScreen extends StatefulWidget {
  MapScreen();

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController mapController;
  Future<List<Store>> stores;
  Future<LocationData> locationData;
  Set<Marker> markers = Set<Marker>();
  Future<List<Store>> nearestProducts;

  void setMapStyle(String mapStyle) {
    mapController.setMapStyle(mapStyle);
  }

  _onMapCreated(GoogleMapController controller) async {
    mapController = controller;
    //changeMapMode();
  }

  _moveToStore(Store store) {
    mapController.animateCamera(
      CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(store.lat, store.long), zoom: 16.0)),
    );
  }

  _buildCarousel(width, stores) {
    if (stores.isEmpty) {
      return Container();
    }
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        width: width,
        height: width * 0.5,
        padding: const EdgeInsets.only(bottom: 20, top: 10),
        child: Swiper(
          viewportFraction: 0.6,
          scale: 0.8,
          itemBuilder: (context, index) {
            return StoreBanner(
                store: stores[index], width: width, moveToStore: _moveToStore);
          },
          itemCount: stores.length,
          onIndexChanged: (index) {
            setState(() {
              markers.clear();
              markers.add(Marker(
                  markerId: MarkerId('${stores[index].id}'),
                  infoWindow:
                      InfoWindow(title: stores[index].name, onTap: () {}),
                  position: LatLng(stores[index].lat, stores[index].long)));
            });
          },
        ),
      ),
    );
  }

  _buildEmptyCarousel(width) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        width: width,
        height: width * 0.5,
        padding: const EdgeInsets.only(bottom: 20, top: 10),
        child: Swiper(
          viewportFraction: 0.6,
          scale: 0.8,
          itemBuilder: (context, index) {
            return Skeleton(
              width: width * 0.2,
              height: width * 0.5,
              colors: [
                Colors.grey,
                Colors.grey.withOpacity(0.7),
                Colors.grey.withOpacity(0.3)
              ],
            );
          },
          itemCount: 5,
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    stores = Provider.of<StoreModel>(context, listen: false)
        .getListStore(lang: 'en', page: 1);
  }

  @override
  Widget build(BuildContext context) {
    final ModalRoute<dynamic> parentRoute = ModalRoute.of(context);
    final bool canPop = parentRoute?.canPop ?? false;
    return LayoutBuilder(builder: (context, constraints) {
      var width = constraints.maxWidth;
      return Scaffold(
        body: Stack(
          children: [
            ChangeNotifierProvider<MapModel>(
              create: (context) => MapModel(),
              child: Consumer<MapModel>(
                builder: (context, mapModel, _) {
                  switch (mapModel.viewState) {
                    case ViewState.Loaded:
                      if (markers.isEmpty && mapModel.listStore.isNotEmpty) {
                        markers.add(
                          Marker(
                            markerId:
                                MarkerId(mapModel.listStore[0].id.toString()),
                            infoWindow: InfoWindow(
                                title: mapModel.listStore[0].name,
                                snippet: mapModel.listStore[0].address,
                                onTap: () {}),
                            position: LatLng(
                              mapModel.listStore[0].lat,
                              mapModel.listStore[0].long,
                            ),
                          ),
                        );
                      }

                      return Stack(
                        children: <Widget>[
                          Container(
                            width: width,
                            height: MediaQuery.of(context).size.height,
                            child: GoogleMap(
                              mapType: MapType.normal,
                              initialCameraPosition: CameraPosition(
                                target: LatLng(
                                  mapModel.userLocation.latitude,
                                  mapModel.userLocation.longitude,
                                ),
                                zoom: 11.0,
                              ),
                              myLocationEnabled: true,
                              onMapCreated: _onMapCreated,
                              markers: markers,
                            ),
                          ),
                          if (mapModel.listStore.isNotEmpty)
                            _buildCarousel(
                              constraints.maxWidth,
                              mapModel.listStore,
                            ),
                        ],
                      );

                    case ViewState.Loading:
                      return Stack(
                        children: <Widget>[
                          Container(
                            width: width,
                            height: MediaQuery.of(context).size.height,
                            child: GoogleMap(
                              mapType: MapType.normal,
                              initialCameraPosition: CameraPosition(
                                  target: LatLng(
                                    mapModel.userLocation == null
                                        ? 0.0
                                        : mapModel.userLocation.latitude,
                                    mapModel.userLocation == null
                                        ? 0.0
                                        : mapModel.userLocation.longitude,
                                  ),
                                  zoom: 11.0),
                              myLocationEnabled: true,
                              onMapCreated: _onMapCreated,
                              markers: markers,
                            ),
                          ),
                          _buildEmptyCarousel(width),
                        ],
                      );

                    default:
                      return Container(
                        width: width,
                        height: MediaQuery.of(context).size.height,
                        color: Colors.grey,
                      );
                  }
                },
              ),
            ),
            if (canPop)
              SafeArea(
                child: IconButton(
                  onPressed: Navigator.of(context).pop,
                  icon: const Icon(
                    Icons.close,
                    size: 28,
                  ),
                ),
              ),
          ],
        ),
      );
    });
  }
}
