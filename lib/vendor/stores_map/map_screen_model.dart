import 'package:flutter/cupertino.dart';
import 'package:location/location.dart';
import '../store/store_model.dart';
import '../../services/index.dart';

enum ViewState { Init, Loading, Loaded }

class MapModel extends ChangeNotifier {
  ViewState viewState = ViewState.Init;
  final Services _services = Services();
  List<Store> listStore = [];
  LocationData userLocation;

  setState(ViewState state) {
    viewState = state;
    notifyListeners();
  }

  MapModel() {
    setState(ViewState.Loading);
    getUserLocation().then((value) {
      getListStore(lang: 'en', page: 1);
    });
  }

  Future<void> getUserLocation() async {
    var location = Location();
    userLocation = await location.getLocation();
  }

  void getListStore({lang, page}) async {
    setState(ViewState.Loading);
    List<Store> stores = await _services.getStores(lang: lang, page: page);
    List<Store> lstStores = [];
    stores.forEach((store) {
      if (store.long != null && store.lat != null) {
        lstStores.add(store);
      }
    });
    listStore = lstStores;
    setState(ViewState.Loaded);
  }
}
